#!/bin/sh

prometheus_file="/tmp/asuswrt_custom_metrics.prom"

cat <<EOF > $prometheus_file
node_cpu_temp_celsius `cat /proc/dmu/temperature |  grep -o '[0-9].'`
node_wlan_temp_celsius {interface="eth1", ghz="2.4"} `wl -i eth1 phy_tempsense | awk '{ print $1 * .5 + 20 }'`
node_wlan_temp_celsius {interface="eth2", ghz="5.0"} `wl -i eth2 phy_tempsense | awk '{ print $1 * .5 + 20 }'`
node_wlan_clients_count {interface="eth1", ghz="2.4"} `wl -i eth1 assoclist | awk '{print $2}' | wc -l`
node_wlan_clients_count {interface="eth2", ghz="5.0"} `wl -i eth2 assoclist | awk '{print $2}' | wc -l`
node_wan_pingtest {dest="google.com"} `ping -c1 -W1 google.com | grep 'seq=' | sed 's/.*time=\([0-9]*\.[0-9]*\).*$/\1/'`
node_wan_pingtest {dest="iserver4.tv"} `ping -c1 -W1 iserver4.tv | grep 'seq=' | sed 's/.*time=\([0-9]*\.[0-9]*\).*$/\1/'`
node_wan_pingtest {dest="itcore.lv"} `ping -c1 -W1 itcore.lv | grep 'seq=' | sed 's/.*time=\([0-9]*\.[0-9]*\).*$/\1/'`
EOF
